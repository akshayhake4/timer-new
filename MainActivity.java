package com.example.mytimer;

import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.app.NotificationChannel;
import android.widget.Toast;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity  {

    private static final String CHANNEL_ID = "channel1";
    TextView texthr,textmin,textsec;
    int min=0,sec=0,hr=0;
    String notification_string;
    Timer timer;
    public static final String mypackage="com.example.mytimer";
    long diff,cur_days=0,cur_hr=0,cur_min=0,cur_sec=0,oldtime;
    boolean flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        texthr=findViewById(R.id.texthrs);
        textmin=findViewById(R.id.textmin);
        textsec=findViewById(R.id.textsecond);






        timer=new Timer();
        timer.schedule(new TimerTask() {
           @Override
           public void run() {



               while(true)
               {

                   KeyguardManager myKM = (KeyguardManager) getBaseContext().getSystemService(Context.KEYGUARD_SERVICE);
                   if( myKM.inKeyguardRestrictedInputMode()) {
                       //it is locked

                        if(!flag) {

                            Date date = new Date();

                            /*SharedPreferences sp = getSharedPreferences(mypackage, Context.MODE_PRIVATE);
                            sp.edit().putLong("lasttime", date.getTime()).apply();*/
                            oldtime=date.getTime();
                            hr = min = sec = 0;
                            flag = true;
                        }

                   } else {
                       //it is not locked
                       if(flag) {
                           showNotification();
                           flag=false;
                       }



                       if(sec==59 && min==59 && hr==59) {
                           sec=min=hr=0;
                       }
                       else if(sec==59 && min==59) {
                           sec = min = 0;
                           hr += 1;
                       }
                       else if(sec==59) {
                           sec=0;
                           min+=1;
                       }
                       else {
                           sec+=1;
                       }
                   }







                   textsec.setText(String.valueOf(sec));
                   textmin.setText(String.valueOf(min));
                   texthr.setText(String.valueOf(hr));




                   try {
                       Thread.sleep(1000);
                   } catch (InterruptedException e) {
                       Toast.makeText(MainActivity.this,"Something went wrong....",Toast.LENGTH_LONG).show();
                   }

               }

           }
       },1000);




    }

    public void showNotification() {

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel =new NotificationChannel(CHANNEL_ID,CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.setDescription("Notification");
                NotificationManager notificationManager=getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            long lasttime;

            Date curDate = new Date();
           /* SharedPreferences sp = getSharedPreferences(mypackage, Context.MODE_PRIVATE);
            lasttime = sp.getLong("lasttime", new Date().getTime());*/
           lasttime=oldtime;
            diff = curDate.getTime() - lasttime;
            cur_days = diff / (24 * 60 * 60 * 1000);
            diff -= cur_days * (24 * 60 * 60 * 1000);
            cur_hr = diff / (60 * 60 * 1000);
            diff -= cur_hr * (60 * 60 * 1000);
            cur_min = diff / (60 * 1000);
            diff -= cur_min * (60 * 1000);
            cur_sec = diff / 1000;


                notification_string = "App was closed for  " + cur_days + " Days, " + cur_hr + " Hr, " + cur_min + " Min and " + cur_sec + " Sec";


            NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.time)
                    .setContentTitle("New notification")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(notification_string))
                    .setContentText(notification_string)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManagerCompat;
            notificationManagerCompat = NotificationManagerCompat.from(MainActivity.this);
            notificationManagerCompat.notify(1, builder.build());

    }





}


