App Name: My Timer

Topic : Display notification of time for which device is locked...

Introduction:

This is a just Simple Timer.
When app starts,timer will run.
When power(lock) key is pressed,it starts counting time until device is unlocked and shows notification.

Condition : 
  App should be in running state at the time of locking the device.
